package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "firstnames")
public class FirstNameModel {
	public static final String FIELD_NAME = "firstName";

	@DatabaseField(id = true)
	private int id;

	@DatabaseField(columnName = FIELD_NAME, canBeNull = false)
	private String value;

	public FirstNameModel() {

	}

	public FirstNameModel(int id, String firstName) {
		super();
		this.id = id;
		this.value = firstName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return value;
	}

	public void setFirstName(String firstName) {
		this.value = firstName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FirstNameModel other = (FirstNameModel) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {	
		return value;
	}
}

package model;

import java.sql.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "books")
public class BooksModel {
	public static final String AUTHOR_ID_FIELD_NAME = "authorId";
	@DatabaseField(id = true)
	private int id;

	@DatabaseField()
	private String title;

	@DatabaseField(canBeNull = false)
	private String description;

	@DatabaseField(canBeNull = false)
	private Date realease_date;

	@DatabaseField(canBeNull = false)
	private String coverLink;

	@DatabaseField(canBeNull = false)
	private int volume;

	@DatabaseField(canBeNull = false)
	private String isbn;

	@DatabaseField(foreign = true, columnName = AUTHOR_ID_FIELD_NAME)
	private AuthorModel authorModel;

	public BooksModel() {

	}

	public BooksModel(int id, String title, String description, Date realease_date, String coverLink, int volume,
			String isbn, AuthorModel authorModel) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.realease_date = realease_date;
		this.coverLink = coverLink;
		this.volume = volume;
		this.isbn = isbn;
		this.authorModel = authorModel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getRealease_date() {
		return realease_date;
	}

	public void setRealease_date(Date realease_date) {
		this.realease_date = realease_date;
	}

	public String getCoverLink() {
		return coverLink;
	}

	public void setCoverLink(String coverLink) {
		this.coverLink = coverLink;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public AuthorModel getAuthorModel() {
		return authorModel;
	}

	public void setAuthorModel(AuthorModel authorModel) {
		this.authorModel = authorModel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorModel == null) ? 0 : authorModel.hashCode());
		result = prime * result + ((coverLink == null) ? 0 : coverLink.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + ((realease_date == null) ? 0 : realease_date.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + volume;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BooksModel other = (BooksModel) obj;
		if (authorModel == null) {
			if (other.authorModel != null)
				return false;
		} else if (!authorModel.equals(other.authorModel))
			return false;
		if (coverLink == null) {
			if (other.coverLink != null)
				return false;
		} else if (!coverLink.equals(other.coverLink))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (realease_date == null) {
			if (other.realease_date != null)
				return false;
		} else if (!realease_date.equals(other.realease_date))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (volume != other.volume)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BooksModel [id=" + id + ", title=" + title + ", description=" + description + ", realease_date="
				+ realease_date + ", coverLink=" + coverLink + ", volume=" + volume + ", isbn=" + isbn
				+ ", authorModel=" + authorModel + "]";
	}

}

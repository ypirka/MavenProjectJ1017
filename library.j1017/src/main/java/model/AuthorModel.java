package model;

import java.sql.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "authors")
public class AuthorModel {
	
	public static final String FIRST_NAME_FIELD_NAME = "firstName";
	public static final String MIDDLE_NAME_FIELD_NAME = "middleName";
	public static final String LAST_NAME_FIELD_NAME = "lastName";
	
	@DatabaseField(id = true)
	private int id;
	
	@DatabaseField(foreign = true,canBeNull = false,foreignColumnName = "id" , columnName = FIRST_NAME_FIELD_NAME )
	private FirstNameModel firstNameModel ;
	
	@DatabaseField(foreign = true,canBeNull = false,foreignColumnName = "id" , columnName = MIDDLE_NAME_FIELD_NAME )
	private MiddleNameModel middleNameModel ;
	
	@DatabaseField(foreign = true,canBeNull = false,foreignColumnName = "id" , columnName = LAST_NAME_FIELD_NAME )
	private LastNameModel lastNameModel ;
	
	@DatabaseField(canBeNull = false)
	private Date dateBirth  ;
	
	@DatabaseField(canBeNull = false)
	private Date goneDate ;
	
	@DatabaseField(canBeNull = false)
	private String alias ;
	
	@DatabaseField(canBeNull = false)
	private String genre ;
	
	public AuthorModel() {
		
	}

	public AuthorModel(int id, FirstNameModel firstNameModel, MiddleNameModel middleNameModel,
			LastNameModel lastNameModel, Date dateBirth, Date goneDate, String alias, String genre) {
		super();
		this.id = id;
		this.firstNameModel = firstNameModel;
		this.middleNameModel = middleNameModel;
		this.lastNameModel = lastNameModel;
		this.dateBirth = dateBirth;
		this.goneDate = goneDate;
		this.alias = alias;
		this.genre = genre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FirstNameModel getFirstNameModel() {
		return firstNameModel;
	}

	public void setFirstNameModel(FirstNameModel firstNameModel) {
		this.firstNameModel = firstNameModel;
	}

	public MiddleNameModel getMiddleNameModel() {
		return middleNameModel;
	}

	public void setMiddleNameModel(MiddleNameModel middleNameModel) {
		this.middleNameModel = middleNameModel;
	}

	public LastNameModel getLastNameModel() {
		return lastNameModel;
	}

	public void setLastNameModel(LastNameModel lastNameModel) {
		this.lastNameModel = lastNameModel;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public Date getGoneDate() {
		return goneDate;
	}

	public void setGoneDate(Date goneDate) {
		this.goneDate = goneDate;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "AuthorModel [id=" + id + ", firstNameModel=" + firstNameModel + ", middleNameModel=" + middleNameModel
				+ ", lastNameModel=" + lastNameModel + ", dateBirth=" + dateBirth + ", goneDate=" + goneDate
				+ ", alias=" + alias + ", genre=" + genre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((dateBirth == null) ? 0 : dateBirth.hashCode());
		result = prime * result + ((firstNameModel == null) ? 0 : firstNameModel.hashCode());
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((goneDate == null) ? 0 : goneDate.hashCode());
		result = prime * result + id;
		result = prime * result + ((lastNameModel == null) ? 0 : lastNameModel.hashCode());
		result = prime * result + ((middleNameModel == null) ? 0 : middleNameModel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorModel other = (AuthorModel) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (dateBirth == null) {
			if (other.dateBirth != null)
				return false;
		} else if (!dateBirth.equals(other.dateBirth))
			return false;
		if (firstNameModel == null) {
			if (other.firstNameModel != null)
				return false;
		} else if (!firstNameModel.equals(other.firstNameModel))
			return false;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		if (goneDate == null) {
			if (other.goneDate != null)
				return false;
		} else if (!goneDate.equals(other.goneDate))
			return false;
		if (id != other.id)
			return false;
		if (lastNameModel == null) {
			if (other.lastNameModel != null)
				return false;
		} else if (!lastNameModel.equals(other.lastNameModel))
			return false;
		if (middleNameModel == null) {
			if (other.middleNameModel != null)
				return false;
		} else if (!middleNameModel.equals(other.middleNameModel))
			return false;
		return true;
	}
		
}

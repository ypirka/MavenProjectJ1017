package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import model.AuthorModel;
import model.BooksModel;
import model.FirstNameModel;
import model.LastNameModel;
import model.MiddleNameModel;

import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

public class FilterBooks extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String databaseUrl = "jdbc:mysql://root:admin@localhost:3306/bestlibrary?serverTimezone=UTC";
	private Logger logger;
	private Dao<FirstNameModel, Integer> firstNameDao;
	private Dao<MiddleNameModel, Integer> middleNameDao;
	private Dao<LastNameModel, Integer> lastNameDao;
	private Dao<AuthorModel, Integer> authorDao;
	private Dao<BooksModel, Integer> booksDao;

	public FilterBooks() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		super.init();
		logger = Logger.getLogger(FilterBooks.class);
		ConnectionSource connectionSource = null;
		try {
			connectionSource = new JdbcConnectionSource(databaseUrl);
			logger.debug("DataBase connection established : " + connectionSource.toString());

			firstNameDao = DaoManager.createDao(connectionSource, FirstNameModel.class);
			middleNameDao = DaoManager.createDao(connectionSource, MiddleNameModel.class);
			lastNameDao = DaoManager.createDao(connectionSource, LastNameModel.class);
			authorDao = DaoManager.createDao(connectionSource, AuthorModel.class);
			booksDao = DaoManager.createDao(connectionSource, BooksModel.class);

		} catch (SQLException e) {
			logger.error("DataBase connection error : " + e.getMessage());

		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Failed at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		BufferedReader reader = null;
		reader = request.getReader();
		StringBuffer sb = new StringBuffer();
		String s;
		while ((s = reader.readLine()) != null) {
			sb.append(s);
		}
		Gson gson = new Gson();
		
		FilterRequest filterRequest =
				gson.fromJson(sb.toString(), FilterRequest.class) ;
		logger.info("Got request: " + filterRequest);
		String lastName = filterRequest.author;
		
		QueryBuilder<AuthorModel, Integer> queryAuthorBuilder =
		  authorDao.queryBuilder();
		QueryBuilder<LastNameModel, Integer> queryLastNameBuilder =
			lastNameDao.queryBuilder();	
		try {
			queryLastNameBuilder.where().
				like(LastNameModel.FIELD_NAME, "%"+lastName+"%");
			queryAuthorBuilder.joinOr(queryLastNameBuilder);
			PreparedQuery<AuthorModel> preparedQuery = queryAuthorBuilder.prepare();
			logger.info("preparedQuery statements: " + preparedQuery.getStatement());
			List <AuthorModel> listAuthors = authorDao.query(preparedQuery);
			logger.info(listAuthors);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private static class FilterRequest {
		private String author;
		private String title;
		private String isbn;
		@Override
		public String toString() {
			return "FilterRequest [author=" + author + ", title=" + title + ", isbn=" + isbn + "]";
		}
	
	
	}

}

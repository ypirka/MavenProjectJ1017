<!DOCTYPE html>
<html lang="en">
<head>
    <link type="text/css" rel="stylesheet" href="resources/css/stylesheet.css">
    <meta charset="UTF-8">
    <title>BEST LIBRARY</title>    
    <script type="text/javascript" src = "https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="resources/scripts/jQueryTest.js"></script>    
    <script type="text/javascript" src="resources/scripts/script.js"></script>
	
</head>
<body onload="time()">
<div class="header">
	<div id="logo">
		<img src="resources/images/1.jpg" id="logoImg" >
	</div>
	<div class="title">
		<span id="titleText">
		<qwer></qwer>LIBRARY</span>
		<p style="font-size:12px;">Contacts:</p>
		<p class="contactLine">number: +375-29-000-00-00</p>
		<p class="contactLine">e-mail: www@mail.com</p>
	</div>
</div>
<div class="menu">
	<p class="menuFloat" id="add">ADD </p>
	<p class="menuSeparator">|</p>
	<p class="menuFloat">REMOVE </p>
	<p class="menuSeparator">|</p>
	<p class="menuFloat">EDIT </p>
</div>
<div class="mainContent">
	<div class="search">
		<div class="pDiv">
			<p>Author:</p>
			<p>ISBN: </p>
			<p>Title:</p>
		</div>
		<div class="inputDiv">
			<input type="text" class="inputClass" id="inputAuthor" >
			<input type="text" class="inputClass" id="inputIsbn">
			<input type="text"  class="inputClass" id="inputTitle">
			<input type ="button" value="Search" id = "search">
		</div>
	</div>
	<div id="tables" class="tableBooks">
		
	</div>
</div>
</body>
</html>
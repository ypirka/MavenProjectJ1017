var zooming = true;
var zoomObject;
var urlPosts = "https://jsonplaceholder.typicode.com/users";
var zoomIn = function() {
	if (zooming == true) {
		$(zoomObject).toggleClass("animationZoomIn");
		zooming = false;
	} else {
		$(zoomObject).removeClass("animationZoomIn");
		zooming = true;
	}
}
$(document).ready(function() {
	var timeInt;
	$("p.menuFloat").hover(function() {
		zoomObject = this;
		zoomIn();
		timeInt = setInterval(zoomIn, 200);
	}, function() {
		clearInterval(timeInt);
		zooming = true;
		$("p.menuFloat").removeClass("animationZoomIn");
	})
	$("#add").click(function() {
		$.ajax(urlPosts, {

			dataType : 'json',
			complete : function(jqXHR, textStatus) {
				console.log("Request complete, Status= " + textStatus);
				var responseText = jqXHR.responseText;
				var response = JSON.parse(responseText);
				console.dir(response);
				createTable(response);
			}
		})

	})
})
function createTable(users) {
$("#tables").empty();
	$("#tables").append("<table id = 'userTable'></table>");
	var table = $("#tables table")
	console.log(table);
	for (iterator in users) {
		console.log(iterator + ":" + users[iterator]);
		$(table).append("<tr id='row"+iterator+"'></tr>");
		var tr=$("#row"+iterator);
		//<td> name : Vasya </td>
		//<td> address : Piter </td>
		$(tr).append("<td> name : " + users[iterator].name+ " </td>");
		$(tr).append("<td> address : "+users[iterator].address.city
				+" , "+users[iterator].address.street+" </td>");
		$(tr).append("<td> email : "+users[iterator].email+" </td>");
	}
}
